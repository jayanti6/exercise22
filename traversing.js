function SelectElements(){

}

SelectElements.prototype.findImageAttr = function(){
  $("img").each(function(){
    console.log($(this).attr("alt"));
  });
};

SelectElements.prototype.addClassToForm = function(){
  $("input[class='input_text']").closest("form").addClass("someRandomClass");
};

SelectElements.prototype.addClassToList = function(){
  $("#myList li.current")
    .removeClass("current")
    .next()
    .addClass("current");
};

SelectElements.prototype.traverseToSubmitButton = function(){
  $("#specials select")
    .closest("ul")
    .last()
    .find(".input_submit");
};

SelectElements.prototype.addClassToSlideShow = function(){
  $("#slideshow")
    .find("li:eq(0)")
    .addClass("current")
    .siblings()
    .addClass("disabled");
};
$(document).ready(function(){
  var selectElement = new SelectElements();
  // 1
  selectElement.findImageAttr();
  // 2
  selectElement.addClassToForm();
  // 3
  selectElement.addClassToList();
  // 4
  selectElement.traverseToSubmitButton();
  // 5
  selectElement.addClassToSlideShow();
});